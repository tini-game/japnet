console.log("Starting Redis...");
const redisProc = require("child_process").spawn("redis-server");
const kill = require("kill-with-style");

// Handle Ctrl-C
process.on("SIGINT", function () {
	kill(redisProc.pid);
	console.log("\n'night");
	process.exit();
});