const refresh = "refresh_token",
	code = "authorization_code";

async function handleOAuth(req, res) {
	let grantType;
	if (req.body.type === "code") grantType = code;
	if (req.body.type === "refresh") grantType = refresh;

	// check for refresh token
	
	const tokenLocation = `tokens.${req.cookies.access_token}`;
	const refresh_token = await keyv.get(tokenLocation);

	// make sure the world hasn't ended
	if (
		(grantType === refresh && refresh_token === undefined) ||
		(grantType !== refresh && grantType === undefined)
	) {
		res.statusCode = 502;
		res.json({ error: "what did you do" });
		return;
	}

	// change optional argument based on the request
	const optKey = grantType === refresh ? "refreshToken" : "code";
	const optVal = grantType === refresh ? refresh_token : req.body.code;
	const obj = await oauth
		.tokenRequest({
			[optKey]: optVal,
			scope: "identify",
			grantType,
		})
		.catch(e => {
			console.log(e.message, e.response);
			res.statusCode = 502;
			res.json({ error: "something went wrong with oauth" });
		});

	await keyv.set(`tokens.${obj.access_token}`, obj.refresh_token);

	const user = await oauth.getUser(obj.access_token);
	await keyv.set(`${user.id}.token`, obj.access_token);
	res.cookie("access_token", obj.access_token, {
		secure: true,
		// i doubt this is safe... (100 days)
		maxAge: 8640000000,
	});
	res.statusCode = 200;
	res.json({ message: "got it lol" });

	// old code in case it's needed later

	// if (req.body.type === "code") {
	// 	try {
	// 		oauth
	// 			.tokenRequest({
	// 				code: req.body.code,
	// 				scope: "identify",
	// 				grantType: "authorization_code",
	// 			})
	// 			.then(async obj => {
	// 				await keyv.set(
	// 					`tokens.${obj.access_token}`,
	// 					obj.refresh_token
	// 				);
	// 				// check for old token from user, and delete if present
	// 				const user = await oauth.getUser(obj.access_token);
	// 				const oldToken = await keyv.get(`${user.id}.token`);
	// 				if (oldToken !== undefined) await keyv.delete(oldToken);
	// 				// store token(s)
	// 				await keyv.set(`${user.id}.token`, obj.access_token);
	// 				// send to user
	// 				res.cookie("access_token", obj.access_token, {
	// 					secure: true,
	// 					// 100 days (this is probably bad!)
	// 					maxAge: 8640000000,
	// 				});
	// 				res.statusCode = 200;
	// 				res.json({ message: "got it lol" });
	// 			})
	// 			.catch(e => {
	// 				console.log(e.message, e.response);
	// 				res.statusCode = 502;
	// 				res.json({ error: "something went wrong with oauth" });
	// 			});
	// 	} catch (e) {
	// 		console.log(e);
	// 	}
	// } else if (req.body.type === "refresh") {
	// 	const tokenLocation = `tokens.${req.cookies.access_token}`;
	// 	const refresh_token = await keyv.get(tokenLocation);
	// 	if (refresh_token !== undefined) {
	// 		await keyv.delete(tokenLocation);
	// 		oauth
	// 			.tokenRequest({
	// 				refreshToken: refresh_token,
	// 				scope: "identify",
	// 				grantType: "refresh_token",
	// 			})
	// 			.then(async obj => {
	// 				// TODO: i can *probably* have the same callback for both of these, make into separate func?
	// 				await keyv.set(
	// 					`tokens.${obj.access_token}`,
	// 					obj.refresh_token
	// 				);
	// 				const user = await oauth.getUser(obj.access_token);
	// 				await keyv.set(`${user.id}.token`, obj.access_token);
	// 				res.cookie("access_token", obj.access_token, {
	// 					secure: true,
	// 					maxAge: 8640000000,
	// 				});
	// 				res.statusCode = 200;
	// 				res.json({ message: "got it lol" });
	// 			})
	// 			.catch(e => {
	// 				res.statusCode = 502;
	// 				res.json({ error: "something went wrong with oauth" });
	// 			});
	// 	} else {
	// 		res.statusCode;
	// 	}
	// }
}

module.exports = handleOAuth;