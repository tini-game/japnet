function chatCommandHandler(ws, obj) {
	let args = obj.v.substring(1).split(" ");
	switch (args[0]) {
		case "help":
			ws.send(
				make(
					"chat",
					"Commands\n/help: Lists these.\n/cmpgn: Join a campaign. (/cmpgn [name]) (provide no arguments to list campaigns) \n/tpa: Request to tp to someone. (/tpa [name])\n/tpaccept: Accept someone's tpa request."
				)
			);
			break;
		case "cmpgn":
			if (!basePositions[args[1]]) {
				ws.send(
					make(
						"chat",
						"Campaigns are: " + Object.keys(baseRooms).join(", ")
					)
				);
				break;
			}
			ws.send(
				make("tp", [
					basePositions[args[1]][0],
					basePositions[args[1]][1],
					baseRooms[args[1]][0],
					baseRooms[args[1]][1],
					325,
					1,
					600,
					args[1],
				])
			);
			break;
		case "tpa": {
			const uid = nameToUID(args[1]);
			if (uid === ws.id) {
				ws.send(make("chat", "that's you"));
				break;
			}
			if (!uid) {
				ws.send(make("chat", "never heard of em"));
				break;
			}
			let sent = false;
			wss.clients.forEach(client => {
				if (client.id !== uid) return;
				client.send(
					make(
						"chat",
						`${names[ws.id]} wants to teleport to you! (/tpaccept)`
					)
				);
				sent = true;
				requests[client.id] = ws.id;
			});
			if (sent) ws.send(make("chat", "request sent!"));
			else ws.send(make("chat", "something went wrong"));
			break;
		}
	}
}

const handlers = {
	positions(ws, obj) {
		const [x, y, lc1, lc2, grav] = obj.v;
		if (isNaN(x) || isNaN(y) || isNaN(lc1) || isNaN(lc2) || !!grav !== grav)
			return;

		positions[ws.id] = obj.v;
	},
	chat(ws, obj) {
		if (typeof obj.v !== "string") return;
		if (obj.v == "") return;
		if (obj.v.length > 1000) return;

		messagesThisSecond[ws.id] =
			(messagesThisSecond[ws.id] == undefined
				? messagesThisSecond[ws.id]
				: 0) + 1;
		if (messagesThisSecond[ws.id] > 5) {
			ws.send(make("reload", 0));
		} else if (obj.v.startsWith("/")) {
			chatCommandHandler(ws, obj);
		} else {
			wss.clients.forEach(client => {
				client.send(
					make("chat", `${names[ws.id]}: ${obj.v.substring(0, 1000)}`)
				);
			});
		}
	},
	tpaccept(ws, obj) {
		const to = [
			positions[ws.id][0],
			positions[ws.id][1],
			positions[ws.id][2],
			positions[ws.id][3],
			...obj.v,
		];
		if (requests[ws.id]) {
			wss.clients.forEach(client => {
				if (client.id === requests[ws.id]) {
					client.send(make("tp", to));
					delete requests[ws.id];
				}
			});
		} else ws.send(make("chat", "what"));
	}
};

function handleMessage(ws, data) {
	let obj = {};
	try {
		obj = JSON.parse(data);
	} catch (e) {}
	if (handlers[obj.k]) {
		handlers[obj.k](ws, obj);
	}
}

module.exports = handleMessage;