const mobileConfig = {
	main: [
		[
			{ key: "KeyA", disp: "←" },
			{ key: "KeyD", disp: "→" },
		],
	],
	left: [
		[
			{ key: "Enter", disp: "💬" }
		],
		[
			{ key: "KeyW", disp: "↑" },
			{ key: "Tab", disp: "👥" },
		],
	],
	more: [
		["Reset Position", "r", "KeyR", false],
		["Return to Hub", "Shift + r", "KeyR", true],
		["Toggle Chat", "t", "KeyT", false][
			("Toggle Collision", "o", "KeyO", false)
		],
		["Credits", "c", "KeyC", false],
		["Wipe Save", "Delete", "Delete", false],
	],
};
