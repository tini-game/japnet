const { Server } = require("ws");
const express = require("express");
const app = express();
const https = require("https");
const shortid = require("shortid");
global.PPS = require("./www/shared/globalVariables.js").PPS;
require("dotenv").config();
const DiscordOauth2 = require("discord-oauth2");
const { readFileSync } = require("fs");
global.oauth = new DiscordOauth2({
	clientId: process.env.CLIENT_ID,
	clientSecret: process.env.CLIENT_SECRET,
	redirectUri: process.env.REDIRECT
});

require("./helper/redisControl.js");

const Keyv = require("@keyvhq/core");
const KeyvRedis = require("@keyvhq/redis");
global.keyv = new Keyv({ store: new KeyvRedis("redis://localhost:6379") });
keyv.on("error", err => console.log("Connection Error", err));

// https://discord.com/api/oauth2/authorize?client_id=894608690047488000&redirect_uri=https%3A%2F%2Fgamong2.yhvr.me%3A6969%2Flanding.html&response_type=code&scope=identify
// https://discord.com/api/oauth2/authorize?client_id=894608690047488000&redirect_uri=https%3A%2F%2Flocalhost%3A8000%2Flanding.html&response_type=code&scope=identify

const { parse } = require("cookie");
const handleMessage = require("./helper/messageHandler")

global.wss = new Server({ noServer: true });

global.basePositions = {
	main: [1 * 50 + 25, 6 * 50 + 25],
	easy: [1 * 50 + 25, 6 * 50 + 25],
	alt: [1 * 50 + 25, 8 * 50 + 25],
	arena: [1 * 50 + 25, 8 * 50 + 25],
	coop: [1 * 50 + 25, 8 * 50 + 25],
};

global.baseRooms = {
	main: [0, 8],
	easy: [0, 8],
	alt: [0, 0],
	arena: [0, 0],
	coop: [0, 0],
};
global.positions = {};
global.names = {};
global.requests = {};
global.messagesThisSecond = {};

global.make = function(k, v) {
	return JSON.stringify({ k, v });
}

global.nameToUID = function(name) {
	for (const uid in names) {
		if (names[uid] === name) return uid;
	}
	return false;
}

wss.on("connection", async (ws, req) => {
	let access_token;
	try {
		access_token = parse(req.headers.cookie).access_token;
	} catch (e) { /* broo */ }
	if (!access_token) {
		ws.send(make("reload", "Please log in with Discord!"));
		// ws.terminate();
	} else {
		ws.id = shortid.generate();
		// init stuff, ig
		let { username: name } = await oauth.getUser(access_token);

		while (name.match(/[^a-z0-9_-]/iu)) {
			name = name.replace(/[^a-z0-9_-]/iu, "");
		}

		name = name.substring(0, 32);

		let isDuplicate = Object.values(names).some(nm => name === nm);
		while (isDuplicate) {
			name += "_"
			isDuplicate = Object.values(names).some(nm => name === nm);
		}


		positions[ws.id] = [0, 0, 0, 0, false];
		names[ws.id] = name;
		ws.send(make("name", name));

		wss.clients.forEach(client => {
			client.send(make("names", names));
		});

		wss.clients.forEach(client => {
			client.send(make("chat", `${name} joined the server`));
		});

		ws.send(
			make(
				"chat",
				"Welcome to JAPNet! Type '/help' for a list of commands."
			)
		);
	}

	ws.on("message", data => {
		handleMessage(ws, data);
	});

	ws.on("close", () => {
		wss.clients.forEach(client => {
			client.send(make("goodbye", ws.id));
		});

		wss.clients.forEach(client => {
			client.send(make("chat", `${names[ws.id]} left the server`));
		});
		delete positions[ws.id];
		delete names[ws.id];
		ws.terminate();
	});
});

const server = https.createServer({
	key: readFileSync("./localhost-key.pem"),
	cert: readFileSync("./localhost.pem")
}, app);
server.listen(8000);

server.on("upgrade", (request, socket, head) => {
	wss.handleUpgrade(request, socket, head, socket => {
		wss.emit("connection", socket, request);
	});
});

setInterval(() => {
	wss.clients.forEach(client => {
		let tmp = { ...positions };
		delete tmp[client.id];
		client.send(make("positions", tmp));
	});
}, 1000 / PPS);

setInterval(() => {
	messagesThisSecond = {};
}, 1000);

app.use(express.static("www"));
app.use(express.json());
app.use(require("cookie-parser")())


const handleOAuth = require("./helper/discordOAuth.js")
app.post("/login/discord", (req, res) => {
	handleOAuth(req, res);
});