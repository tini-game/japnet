<p align="center">
  <img src="server/www/pics/japnet.png"><br>
  <em>An open-source MMO platformer.</em>
</p>

Contributions welcome, I'm not sure what else to put here :)

Oh yeah, something probably important--**if you want this to work, you need [redis](https://redis.io/) (`redis-server`) installed globally!**
- **Don't download it from the website**, install if from a package manager so that the `redis-server` command is global.